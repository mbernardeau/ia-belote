package belote.moteur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import belote.ia.BeloteIA;
import belote.ia.ResultNotYetAvailableException;
import util.Tuple;

public class IA {
	private List<Carte> playedCards;
	private Belote belote;
	private Map<Carte, Tuple<Integer,Integer>> cartesChoisies;
	private ArrayList<BeloteIA> workers;
	private ArrayList<Boolean> checkedWorkers;
	private Carte carteChoisie;
	private List<Carte> cartesPossibles ;

	private String nom;

	private static int MAX_EXEC_TIME = 45*1000; //10 secondes
	private int NB_CORES;
	private static int NB_TASK = 80;

	private static int SCORE_ATOUT = 35;
	private static int ACCEPT = NB_TASK/8 ; //valeur de seuil pour le choix anticipé de la meilleur carte
	private static int ACCEPT_MAX_VAL = 150;//5000
	int [] DEPTHS = { 17, 21, 19, 23}; 

	public IA(Belote belote, String nom)
	{
		this.nom = nom;
		this.belote = belote;
		this.playedCards = belote.cartesJouees;	
		this.cartesChoisies = new HashMap<Carte, Tuple<Integer,Integer>>();
		this.workers = new ArrayList<BeloteIA>();
		this.checkedWorkers = new ArrayList<Boolean>();
		this.cartesPossibles = new ArrayList<Carte>();
		this.NB_CORES = Runtime.getRuntime().availableProcessors();
	}
	
	public String askCarte(int id){
		List<Carte> cartesSurLaTable = new ArrayList<Carte>();
		int i = belote.numVainqueurPli;
		Carte c = belote.table.get(belote.joueurs.get(i));
		while(c != null)
		{				
				cartesSurLaTable.add(c);
				if(--i == -1) i =3;
				c = belote.table.get(belote.joueurs.get(i));
		}
		this.cartesPossibles.clear();
		this.cartesChoisies.clear();
		this.workers.clear();
		this.checkedWorkers.clear();

		this.playedCards = belote.cartesJouees;
		List<Carte> joueurMain  = belote.joueurs.get(id).getMain();
		this.cartesPossibles = belote.getCartesValides(joueurMain, belote.joueurs.get(id));
		//convertCards(id);
		
		System.out.print("Joueur : " + nom + " \n");
		System.out.print("Main : taille "+joueurMain.size() +"\n{");
		for(Carte cart : joueurMain ){
			System.out.print(cart + " , " );
		}
		System.out.println("}");
		System.out.print("Possible : \n{");
		for(Carte cart : cartesPossibles ){
			System.out.print(cart + " , " );
		}
		System.out.println("}");
		
		System.out.print("Cartes deja jouées : " + " \n{");

		for(Carte cart : playedCards ){
			System.out.print(cart + " , " );
		}
		System.out.println("}");
		
		carteChoisie = cartesPossibles.get(new Random().nextInt(cartesPossibles.size()));
		if(cartesPossibles.size() > 1){
			return processIA(id, cartesSurLaTable);
		}else{
			try {
				//5 sec de traitement sinon trop rapide...
				Thread.sleep(new Random().nextInt(5000)); //max 5s
			} catch (InterruptedException e) {}
			return carteChoisie.toString();
		}
	}
	
	
	public String askCouleurAtout(int id){
		Joueur joueur = belote.joueurs.get(id);
		
		Map<Couleur, Integer> choice = getScoreByColor(joueur.getMain(), belote.carteProposee, false);
		//belote.carteProposee.getCouleur();
		int best = 0;
		Couleur choix = null; 
		//System.out.println("Tour 2 :Joueur : "+joueur.getNom()+ "");
	    for(Map.Entry<Couleur, Integer> entry : choice.entrySet())
	    {
	    	  //System.out.println(entry.getKey().toString() + " Score : " + entry.getValue() ) ;
	          if(best <= entry.getValue())
	          {
	              best = entry.getValue();
	              choix = entry.getKey();
	          }
	    }
	    if(best >= SCORE_ATOUT){
			System.out.println("Joueur : "+joueur.getNom()+ " je prends l'atout de :" + choix);

	    	return choix.toString();
	    }
		return null;
	}

	public boolean askPrendreAtout(int id){
		Joueur joueur = belote.joueurs.get(id);
		Map<Couleur, Integer> choice = getScoreByColor(joueur.getMain(), belote.carteProposee, true);
	
		Integer res = choice.get(belote.carteProposee.getCouleur());
		if(res != null && res >= SCORE_ATOUT){
			System.out.println("Joueur : "+joueur.getNom()+ " je prends l'atout de : " + belote.carteProposee.getCouleur() + " ( score : " +res+")");
			return true;
		
		}
		return false;
	}
	

	private String processIA(int id, List<Carte> cartesSurLaTable){
		Joueur joueur = belote.joueurs.get(id);
		
		ExecutorService executor = Executors.newFixedThreadPool(NB_CORES);
		System.out.println("Using up to " + NB_CORES + " cores | Tasks :" + NB_TASK);
		long startTime = System.currentTimeMillis();
		long spentTime = 0;
		
		for(int i=0 ; i<NB_TASK ; i++){
			
			Joueur temp= new Joueur(nom, id);
			for(Carte carte : joueur.getMain()) temp.getMain().add(new Carte(carte.getValeur(), carte.getCouleur()));
			
			BeloteIA worker = new BeloteIA(temp, playedCards, belote.atout, cartesSurLaTable, DEPTHS[i%(DEPTHS.length)]);
			workers.add(worker);
			checkedWorkers.add(false);
            executor.execute(worker);
		}
		//start threads
		executor.shutdown();
		while(true)
		{
			try
			{	
				spentTime = System.currentTimeMillis() - startTime;
				System.out.print(spentTime/(1000) + "/" + MAX_EXEC_TIME/(1000) + "  ");
			    Thread.sleep(MAX_EXEC_TIME/10);  
			    
				if(executor.isTerminated()){
					System.out.println("\nAll Tasks finished !");
					break;
				}
				if(spentTime >= MAX_EXEC_TIME){
					//workers.removeAll(
							executor.shutdownNow();
							//);
					System.out.println("\nMax time !");
					break;
				}
			    if(checkAvaibility(false) && processCurrentState()){
			    	//workers.removeAll(
					executor.shutdownNow();
					//);
					//System.out.println("Found good result before end!");
					break;		
			    }
			    
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			    break;
			}
		}
		
		executor.shutdownNow();
		if(executor.isTerminated()){
			System.out.println("All terminated correctly");
		}
		if(checkAvaibility(true)){
			processVote();
		}else{
			System.out.println("Aucun thread n'a reussi a finir correctement");
		}
		
		System.out.println("==> carte choisie : " + this.carteChoisie.toString());
		System.out.println("//////");
		
		
		if (!cartesPossibles.contains(carteChoisie)){
			System.out.println("Erreur !!!! carte choisie non possible");
			return 	cartesPossibles.get(new Random().nextInt(cartesPossibles.size())).toString();
		}
		return this.carteChoisie.toString();
	}
	
	//returns if last => returns true if cartesChoisies is not empty
	//if not last => returns true if a card has been added to cartesChoisies;
	private synchronized boolean checkAvaibility(boolean last){
		int counter = 0,counterAlre=0;
		if(this.workers.size() > 0)
		{
			int size = this.workers.size()-1;
			for(int index = size ; index >= 0 ; index--){
				BeloteIA current = this.workers.get(index);
				Boolean checked = this.checkedWorkers.get(index);
				if(!checked){
					try {
						if(current.wasRunned()){
							Tuple<Carte,Integer> result = current.getNextCardToPlay();
							Carte carte = result.x;
							if(carte != null){
								Tuple<Integer,Integer> i = cartesChoisies.get(carte);
							    if (i == null) {
							    	cartesChoisies.put(carte, new Tuple<Integer,Integer>(1, result.y));
				                } else {
				                	i.x++; i.y += result.y;
				                	cartesChoisies.put(carte, i);
				                }
								//this.workers.remove(index);
								this.checkedWorkers.set(index, true);
								counter++;
							}else{
								System.out.println("Carte null for " + result.x);
							}
						}
					} catch (ResultNotYetAvailableException e) {
						System.out.println("Exception");//e.printStackTrace();
					}
				}else{
					//counterAlre++;
				}
			}			
		}
		//System.out.println("Added " + counter + " Cards... ");//Already checked " + counterAlre);
		return last ? !cartesChoisies.isEmpty() : counter != 0;
	}
		
	private void processVote(){
		int best = 0, bestMaxVal =0;
		for(Map.Entry<Carte, Tuple<Integer,Integer>> entry : this.cartesChoisies.entrySet())
	    {
			Tuple<Integer,Integer> current = entry.getValue();
	    	 System.out.println("Carte : " + String.format("%-15s", entry.getKey().toString())+ " \t\t Nb : " + current.x + " Sum Max : " + current.y ) ;
	    	 if((best < current.x) || (current.x+ACCEPT>best && (bestMaxVal < current.y)))
	          {
	              best = current.x;
	              bestMaxVal = current.y;
	              this.carteChoisie = entry.getKey();
	          }
	    }		
	}
	
	
	private boolean processCurrentState(){
			int best_value=0 , last_best_value= 0;
			int best_max=0 , last_best_max= 0;
			
			for(Map.Entry<Carte, Tuple<Integer,Integer>> entry : this.cartesChoisies.entrySet())
		    {
		          if(entry.getValue().y>= last_best_max)
		          {
		        	  if(entry.getValue().y >= best_max){
		        		  last_best_max = best_max;
		        		  best_max = entry.getValue().y;
		        	  }else{
		        		  last_best_max = entry.getValue().y;
		        	  }
		        
		          }
		          
		          if(entry.getValue().x >= last_best_value)
		          {
		        	  if(entry.getValue().x >= best_value){
			        	  last_best_value = best_value;
			        	  best_value = entry.getValue().x;
		        	  }else{
		        		  last_best_value = entry.getValue().x;
		        	  }
		          }
		    }
			
			if((last_best_value+ACCEPT) <= (best_value)){
				//System.out.println((last_best_value+ACCEPT) + " / " + best_value);
				System.out.println("\nBest card has been choosen " + ACCEPT + " times more than others");
				return true;
			}
			
			if((last_best_max+ACCEPT_MAX_VAL) < (best_max) && (last_best_value+ACCEPT/4) <= (best_value)){
				//System.out.println((last_best_max+ACCEPT_MAX_VAL) + " / " + best_max);
				System.out.println("\nBest Card  has maxVal sum +" + ACCEPT_MAX_VAL + " more than others");
				return true;
			}
			
			
			if((last_best_value+ACCEPT/2) <= (best_value) && (last_best_max+ACCEPT_MAX_VAL/2) < (best_max)){
				System.out.println("\nBest Card has maxVal and choosen");
				return true;
			}
						
		return false;
	}

	
	private void convertCards(int id){
		System.out.println("Id IA :"+id);
		List<Carte> joueurMain  = belote.joueurs.get(id).getMain();
		List<Carte> cartesValides = belote.getCartesValides(joueurMain, belote.joueurs.get(id));

		for (Carte currCarte : cartesValides){
			if(cartesPossibles.contains(currCarte)) {
				System.out.println("Erreur !!!! card already in players hands...");
				continue;
			}
			if(playedCards.contains(currCarte)){
				System.out.println("Erreur !!!! card already played in this game...");
				continue;
			}
			if(!joueurMain.contains(currCarte)){
				System.out.println("Erreur !!!! card  is not supposed to be in players hands...");
				continue;
			}
			cartesPossibles.add(currCarte);
		}
	}
		
	//Retourne les score de la main par couleur en considérant avoir prit l'atout (pour chaque couleur) 
	private Map<Couleur, Integer> getScoreByColor(List<Carte> main, Carte carteProposee, boolean atout){
		Map<Couleur, Integer> scoreByColor = new HashMap<Couleur, Integer>();
		//main.add(carteProposee);
		for(int k =0 ; k <=main.size() ; k++){
			Carte carte;
			if(k == main.size())
				carte = carteProposee;
			else
				carte = main.get(k);
						
			Couleur couleur = carte.getCouleur();
			//quand atout  = vrai : on suppose que le score est si atout couleur == atout
			int score;
			if(atout)
				score = carte.getValeur(carteProposee.getCouleur());
			else
				score = carte.getValeur(couleur); 
			
            Integer i = scoreByColor.get(couleur);

		    if (i == null) {
		    	scoreByColor.put(couleur, score);
            } else {
            	scoreByColor.put(couleur, i+score);
            }
		}
		
		
		return scoreByColor;
	}
}
