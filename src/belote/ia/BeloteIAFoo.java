package belote.ia;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import belote.moteur.Belote;
import belote.moteur.Carte;
import belote.moteur.Couleur;
import belote.moteur.Joueur;

public class BeloteIAFoo  extends BeloteIA implements Runnable {


	private int TaskId;
	private Carte carteSelectionnee;
	//TODO Joueur n'est pas utile ici
	//TODO a voir si il le sera dans le vrai belotteIA...
	public BeloteIAFoo(Joueur joueur, List<Carte> alreadyPlayedCards, List<Carte> cartesPossibles, int task){
		super(joueur, alreadyPlayedCards, Couleur.CARREAU, new ArrayList<Carte>(), 0);
		
		TaskId = task;		
		carteSelectionnee= cartesPossibles.get(new Random().nextInt(cartesPossibles.size()));

		//retour carte aleatoire
	}
	
	@Override
	public void run() {
		//System.out.println(Thread.currentThread().getName() + "Start : task =>" + TaskId );
		isRunning = true;
		wasRunned = true;
		try {
			//10 sec de traitement
			Thread.sleep(new Random().nextInt(10000)); //max 10s
		} catch (InterruptedException e) {
		}
		this.bestCard = carteSelectionnee;
		isRunning = false;
		System.out.println(TaskId+" :\t\t\t choix carte : " + this.bestCard);
	}
	
	

}
