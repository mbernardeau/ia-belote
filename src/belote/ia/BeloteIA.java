package belote.ia;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import belote.moteur.Belote;
import belote.moteur.Carte;
import belote.moteur.Couleur;
import belote.moteur.Joueur;
import belote.moteur.PaquetCartes;
import util.Tuple;

public class BeloteIA extends Belote implements Runnable {
	private class Pli extends ArrayList<Carte>{
		private Joueur winner;
		public boolean add(Carte e, Joueur j){
			if(this.size() == 0){
				couleurPli = e.getCouleur();
				winner = j;
				vainqueurPli = j;
			}else if(this.size() > 0 && e.isSuperieur(table.get(vainqueurPli), atout, couleurPli)){
				winner = j;
				vainqueurPli = j;
			}
			table.put(j, e);
			return super.add(e);
		}

		boolean isOver(){
			return this.size() == 4;
		}
		
		int nextPlayer(){
			if(isOver())
				return -1;
			else
				return this.size();
		}

		@Override
		public String toString() {
			String res = "Pli [";
			for(Carte c : this)
				res += c;
			return res + "]";
		}
		
		
		
	}
	private Stack<Pli> pilePlis = new Stack<Pli>();
	

	protected int DEPTH = 16;
	//TODO change to private after test
	protected boolean isRunning = false;
	protected boolean wasRunned = false;
	protected Carte bestCard;
	private int max;
	
	public synchronized boolean wasRunned() {
		return wasRunned;
	}


	private Joueur IAPlayer;
	private List<Carte> cardsOnTable;
	public BeloteIA(Joueur joueur, List<Carte> alreadyPlayedCards, Couleur atout, List<Carte> cardsOnTable, int depth){
		super();
		if(depth != 0)DEPTH = depth;
		
		IAPlayer = joueur;
		
		for(int i = 0; i<4; i++){
			if(i != cardsOnTable.size())
				joueurs.add(new Joueur("Adversary"+i, i));
			else
				joueurs.add(joueur);
		}
		
		this.cardsOnTable = cardsOnTable;
		// Adding random cards among the remaining ones (not in the player's hand) for each adversary
		PaquetCartes paquet = new PaquetCartes();
		paquet.removeAll(joueur.getMain());
		paquet.removeAll(alreadyPlayedCards);
		paquet.removeAll(cardsOnTable);
		paquet.melanger();
		int nbCardsToDistribute = paquet.size()/3;
		for(Joueur j : joueurs){
			if(j != IAPlayer){
				j.getMain().addAll(paquet.piocher(nbCardsToDistribute));
				paquet.retirer(nbCardsToDistribute);
			}
		}
		this.atout = atout;
	}

	@Override
	public void run() {
		isRunning = true;
		Pli pli = new Pli();
		int j = 0;
				
		for(Carte c : cardsOnTable){
			pli.add(c, joueurs.get(j++));
		}
		
		//System.out.println("First Player:"+pli.nextPlayer());
		
		
		// Perform calculation here

		max = maxValue(pli, DEPTH, Integer.MIN_VALUE, Integer.MAX_VALUE);

		//System.out.println("Max value:"+max);
		while(!pilePlis.isEmpty())
			System.out.println(pilePlis.pop());
		isRunning = false;
		wasRunned = true;
		//System.out.println(Thread.currentThread().getName() + " :: Max value:"+max);	
	}

	public Tuple<Carte,Integer> getNextCardToPlay() throws ResultNotYetAvailableException{
		if(this.isRunning){
			throw new ResultNotYetAvailableException("Thread is still running.");
		}else if(!this.wasRunned){
			throw new ResultNotYetAvailableException("Thread was not launched.");
		}else{
			return new Tuple<Carte,Integer>(bestCard, (max*DEPTH) /100);
		}
	}
	

	
	@Override
	protected void tourDeTable(int numVainqueurPliPrecedent){
		
	}
	
	private Pli setPli(Pli pli){
		if(pli.isOver()){
			pli.winner.getCartesGagnees().addAll(pli);
			couleurPli = null;
			table.clear();
			pli = new Pli();
		}
		return pli;
	}
	
	int maxValue(Pli pli, int limit, int alpha, int beta){
		if(gameOver() || limit == 0)
			return IAPlayer.getNbPointsGagnes(atout) + joueurs.get(2).getNbPointsGagnes(atout);
		else
			pli = setPli(pli);
		
		Joueur nextPlayer = joueurs.get(pli.nextPlayer());
		if(nextPlayer.getMain().size() == 0){
			return IAPlayer.getNbPointsGagnes(atout);
		}
		int v = Integer.MIN_VALUE;
		
		List<Carte> possibleCards = getCartesValides(nextPlayer.getMain(), nextPlayer);
		/*if(nextPlayer == IAPlayer && limit > DEPTH-1){
			System.out.print("Cartes possibles: [");
			for( Carte c : possibleCards){
				System.out.print(c+", ");
			}
			System.out.print("]\n");
		}*/
		
		
		for(Carte c : possibleCards){
			pli.add(c, nextPlayer);
			nextPlayer.getMain().remove(c);
			int value;
			
			if(pli.isOver() && (pli.winner == IAPlayer || pli.winner != joueurs.get((joueurs.indexOf(IAPlayer)+2)%4)))
				value = maxValue(pli, limit-1, alpha, beta);
			else
				value = minValue(pli, limit-1, alpha, beta);
			
			if(limit > DEPTH-1 && nextPlayer == IAPlayer && value > v)
				bestCard = c;
			
			v = Math.max(v, value);
			pli.winner.getCartesGagnees().removeAll(pli);
			
			pli.remove(c);
			nextPlayer.getMain().add(c);
			if(v >= beta)
				break;
			alpha = Math.max(alpha, v);
		}
		return v;
	}
	
	int minValue(Pli pli, int limit, int alpha, int beta){
		if(gameOver() || limit == 0 )
			return IAPlayer.getNbPointsGagnes(atout) + joueurs.get(2).getNbPointsGagnes(atout);
		else
			pli = setPli(pli);
		
		Joueur nextPlayer = joueurs.get(pli.nextPlayer());
		if(nextPlayer.getMain().size() == 0){
			return IAPlayer.getNbPointsGagnes(atout);
		}
		
		int v = Integer.MAX_VALUE;
		
		List<Carte> possibleCards = getCartesValides(nextPlayer.getMain(), nextPlayer);

		for(Carte c : possibleCards){
			pli.add(c, nextPlayer);
			nextPlayer.getMain().remove(c);
			
			if(pli.isOver() && pli.winner != IAPlayer && pli.winner != joueurs.get((joueurs.indexOf(IAPlayer)+2)%4)){
				v = Math.min(v, minValue(pli, limit-1, alpha, beta));
			}else{
				v = Math.min(v, maxValue(pli, limit-1, alpha, beta));
			}

			pli.winner.getCartesGagnees().removeAll(pli);
			pli.remove(c);
			nextPlayer.getMain().add(c);
			if(v <= alpha)
				break;
			beta = Math.min(beta, v);
		}
		return v;
	}
	

	boolean gameOver(){
		return IAPlayer.getMain().isEmpty();
	}
}
