package belote.ia;

public class ResultNotYetAvailableException extends Exception {

	public ResultNotYetAvailableException(String string) {
		super(string);
	}

}
