package belote;

import java.util.HashMap;

import util.XML;
import belote.ihm.Controleur;
import belote.ihm.JoueurIA;
import belote.moteur.Belote;

public class MainJoueurIA {
	public static void main(String[] args) throws Exception
	{
		// Chargement de la config
		HashMap config = (HashMap) XML.load("res/ConfigBelote");
		
		// Creation du serveur
		Belote belote = new Belote();
		new Serveur((Integer) config.get("port"), belote);
		
		// Recuperation du nom du joueur
		
		// Creation du joueur aleatoire
		new Client(config, new JoueurIA("Joueur IA 1", false, belote));
		new Client(config, new JoueurIA("Joueur IA 2", true , belote));
		new Client(config, new JoueurIA("Joueur IA 3", true, belote));
		//new Client(config, new Controleur(config));

	}

}
