package belote.ihm;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import belote.moteur.Belote;
import belote.moteur.Carte;
import belote.moteur.IA;


public class JoueurIA extends Console implements Vue
{		
	
	IA IAMoteur;
	List<Carte> alreadyPlayedCards; 

	public JoueurIA(String nom, boolean mute, Belote belote)
	{
		super(nom, mute);
		num = 0;
		IAMoteur = new IA(belote, nom);
	}
	
	public JoueurIA(String nom, Belote belote)
	{
		this(nom, true, belote);
		num=0; 
	}
	
	
	public String askNom()
	{
		return nom;
	}

	public String askCarte(ArrayList<String> main)
	{
		return IAMoteur.askCarte(this.num);
	}

	public boolean askPrendreAtout()
	{
		return IAMoteur.askPrendreAtout(this.num);
	}

	public String askCouleurAtout()
	{
		return IAMoteur.askCouleurAtout(this.num);
	}
	

	
}
