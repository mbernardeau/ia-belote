/**
 * 
 */
package belote.test;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import belote.ia.BeloteIA;
import belote.ia.ResultNotYetAvailableException;
import belote.moteur.Carte;
import belote.moteur.Couleur;
import belote.moteur.Joueur;
import belote.moteur.Valeur;
/**
 * @author Mathias
 *
 */
public class BeloteIATest {

	public BeloteIA belote;
	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Joueur joueur = new Joueur("IAPlayer", 0);
		//joueur.getMain().add(new Carte(Valeur.AS, Couleur.CARREAU));
		joueur.getMain().add(new Carte(Valeur.VALET, Couleur.CARREAU));
		joueur.getMain().add(new Carte(Valeur.NEUF, Couleur.CARREAU));
		joueur.getMain().add(new Carte(Valeur.AS, Couleur.TREFLE));
		joueur.getMain().add(new Carte(Valeur.SEPT, Couleur.CARREAU));
		joueur.getMain().add(new Carte(Valeur.DIX, Couleur.TREFLE));
		joueur.getMain().add(new Carte(Valeur.SEPT, Couleur.COEUR));
		joueur.getMain().add(new Carte(Valeur.AS, Couleur.COEUR));
		List<Carte> alreadyPlayedCards = new ArrayList<Carte>();
		alreadyPlayedCards.add(new Carte(Valeur.AS, Couleur.CARREAU));
		alreadyPlayedCards.add(new Carte(Valeur.HUIT, Couleur.CARREAU));
		alreadyPlayedCards.add(new Carte(Valeur.DIX, Couleur.CARREAU));
		alreadyPlayedCards.add(new Carte(Valeur.HUIT, Couleur.TREFLE));
		List<Carte> table = new ArrayList<Carte>();
		table.add(new Carte(Valeur.AS, Couleur.PIQUE));
		
		belote = new BeloteIA(joueur, alreadyPlayedCards, Couleur.CARREAU, table);
	}

	/**
	 * Test method for {@link belote.ia.BeloteIA#tourDeTable(int)}.
	 */
	@Test
	public void testTourDeTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link belote.ia.BeloteIA#run()}.
	 * @throws ResultNotYetAvailableException 
	 */
	@Test
	public void testRun() throws ResultNotYetAvailableException {
		belote.run();
		System.out.println("Card to play: "+belote.getNextCardToPlay());
	}

	/**
	 * Test method for {@link belote.ia.BeloteIA#getNextCardToPlay()}.
	 */
	@Test
	public void testGetNextCardToPlay() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link belote.ia.BeloteIA#gameOver()}.
	 */
	@Test
	public void testGameOver() {
		fail("Not yet implemented");
	}

}
